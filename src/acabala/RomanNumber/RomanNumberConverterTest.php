<?php
/**
 * Created by PhpStorm.
 * User: Adrian
 * Date: 12.05.14
 * Time: 18:27
 */

namespace acabala\RomanNumber;

require_once('RomanNumberConverter.php');

use \PHPUnit_Framework_TestCase as TestCase;

class RomanNumberConverterTest extends TestCase
{

    /**
     * @var RomanNumberConverter
     */
    private $converter;

    public function setUp()
    {
        $this->converter = new RomanNumberConverter();
    }

    public function testConverterReturnsString()
    {
        $result = $this->converter->arabicToRoman(1);
        $this->assertInternalType('string', $result);
    }

    /**
     * @dataProvider romanNumbersProvider
     */
    public function testReturnsRomanOutputForArabicInput($input, $output)
    {
        $result = $this->converter->arabicToRoman($input);
        $this->assertEquals($output, $result);
    }

    public function romanNumbersProvider()
    {
        return array(
            array(1, 'I'),
            array(2, 'II'),
            array(3, 'III'),
            array(4, 'IV'),
            array(5, 'V'),
            array(6, 'VI'),
            array(7, 'VII'),
            array(8, 'VIII'),
            array(9, 'IX'),
            array(10, 'X'),
            array(20, 'XX'),
            array(99, 'XCIX'),
            array(1903, 'MCMIII'),
            array(1999, 'MCMXCIX'),
        );
    }

    public function testCharsCanBeRepeatedMaxTreeTimes()
    {
        for ($i = 1; $i <= 1599; $i++)
        {
            $result = $this->converter->arabicToRoman($i);
            $this->assertNotRegExp('/([I,X,C,M])\1{3,}/', $result);
        }
    }

    public function testRestrictedCharsCantBeRepeated()
    {
        for ($i = 1; $i <= 1599; $i++)
        {
            $result = $this->converter->arabicToRoman($i);
            $this->assertNotRegExp('/([D,L,V])\1{1,}/', $result);
        }
    }
}
 