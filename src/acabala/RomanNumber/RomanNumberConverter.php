<?php
namespace acabala\RomanNumber;

class RomanNumberConverter
{
    private $roman;
    private $values = [
        1 => 'I',
        4 => 'IV',
        5 => 'V',
        9 => 'IX',
        10 => 'X',
        40 => 'XL',
        50 => 'L',
        90 => 'XC',
        100 => 'C',
        400 => 'CD',
        500 => 'D',
        900 => 'CM',
        1000 => 'M'
    ];

    public function arabicToRoman($number)
    {
        $this->roman = '';
        while ($number > 0) {
            $number = $this->getRomanChar($number);
        }

        return (string) $this->roman;
    }

    private function getRomanChar($number)
    {
        foreach (array_reverse($this->values, true) as $arabic => $roman) {
            if ($number >= $arabic) {
                $this->roman .= $roman;
                $number -= $arabic;
                break;
            }
        }

        return $number;
    }
}
